#+TITLE: Guile Goblins NEWS – history of user-visible changes

* Releases
** v0.11

*** Highlights

 - New time-traveling distributed debugger
 - Substantially improved documentation and docstrings
 - Several more actor-lib modules ported from Racket to Guile
 - Updates to OCapN code

*** New features and improvements
**** Time traveling distributed debugger

Goblins now includes a time traveling distributed debugger!  Yeah, you
heard that right!

The new Goblins debugger allows programmers to inspect and debug
distributed computations that happen across many vats (communicating
event loops.) The time travel feature allows for visiting past events
and inspecting program state at the time the event happened.

These tools are implemented as “meta-commands” for Guile's REPL,
complementing the tools that Guile already provides for sequential
debugging. In true Scheme fashion, this means that debugging happens
live, while the program is running, allowing programmers to fix bugs
and try again without having to stop and recompile/reboot their
program.

Blogpost, with examples:

  https://spritely.institute/news/introducing-a-distributed-debugger-for-goblins-with-time-travel.html

**** Documentation overhaul

Documentation has received a major overhaul in this release:

 - All key procedures now have docstrings, allowing for easier
   discovery of API usage during development
 - The manual's tutorial has been cleaned up for clarity and
   correctness
 - All core API procedures are now documented in the manual
 - All modules in the actor-lib are now documented in the manual
 - The manual now has an index, so finding procedures is much
   easier

**** More actor-lib libraries ported from Racket version of Goblins

 - =(goblins actor-lib pushdown)= is a simple pushdown automata
   implementation, useful for various kinds of state machines,
   especially in some games
 - =(goblins actor-lib opportunistic)= allows for proxying object
   behavior, opportunistically using =$= for near objects but using
   =<-= otherwise (known as select-swear in the Racket version)
 - =(goblins actor-lib simple-mint)= is a very simple example
   "bank" of sorts, ported from the example in
   [[http://erights.org/elib/capability/ode/index.html][An Ode to the Granovetter Diagram]]
 - =(goblins actor-lib let-on)= provides convenient code for promise
   resolution which strongly resembles =let= in Scheme, but with the
   behavior of =on= in Goblins

**** ,vats repl meta-command

It's now possible to list which vats are active via the =,vats=
REPL command.

**** OCapN / CapTP updates

Work has continued on Goblins' implementation of OCapN, the Object
Capability Network, the set of network abstractions which provide
the fluid experience of distributed networked programming.

Goblins' network architecture is still in active development; this
release breaks compatibility with the =0.10= release of Goblins.

***** Semantics closer to CapTP draft specification

Goblins' implementation of the OCapN version of CapTP (the Capability
Transfer Protocol) is aimed to be used as the starting point for
revisions for the OCapN group.  We have submitted a
[[https://github.com/ocapn/ocapn/pull/42][draft version of the protocol]] to the OCapN group.  Goblins 0.11 gets
us closer to the aims of that draft; we are hoping within the next few
releases to get full alignment and group consensus towards a unified
OCapN protocol, and the =0.11-goblins= release of OCapN and CapTP
shipped with Goblins gets us closer to that goal.

***** Add acyclic distributed GC support to CapTP

Goblins 0.11 now ships support for acyclic distributed garbage
collection, meaning that references to cooperative freeing of objects
shared over the network but no longer needed is possible

**** Facets use opportunistic synchronicity now

=(goblins actor-lib facet)= has been updated to use
=(goblins-actor-lib opportunistic)=.  In effect, this means that
the =#:sync?= keyword has been removed; facets now automatically
do the right thing.

*** Bug fixes

**** Backtraces which hung backtrace printing fixed

Previously certain backtraces would break and could hang Goblins.
This was due to a bug in Fibers in conjunction with Guile's backtrace
printer.  We have introduced a workaround which strips the frames
which would break Goblins.  The additional advantage of this fix is
that backtraces printed by Goblins are now significantly cleaner
with irrelevant information reduced.

**** Avoid explosion of threads on creation of new vats

Previously, new schedulers were accidentally made every time
new vats were made.  This would lead to an explosion of threads,
sometimes causing Guile to crash abruptly.  This bug is fixed.

**** Minor fixes

 - Bad reference to ^broken-ben in tutorial fixed
 - Nondeterministic test failures fixed
 - Sealers now print using correct port

*** Deprecations

 - =define-vat-run= is now deprecated, use =call-with-vat= or
   =with-vat= instead.

*** Known issues

 - Two machines simultaneously opening connections to each other on
   the OCapN network through Goblins fail to behave correctly.  This
   is known as the "crossed hellos" problem.  We are hoping to have
   this fixed in the next release.
 - We are still working towards full compatibility with the
   OCapN CapTP draft specification.

*** Thanks

David Thompson
Juliana Sims
Christine Lemmer-Webber
Jessica Tallon (Racket code ported to Goblins)
Vivianne Langdon
Geoffrey J. Teale

** v0.10

*** New features and improvements

**** OCapN support and interoperability with Racket Goblins

Guile Goblins has been updated to speak the OCapN protocol, finally
allowing Guile users to be able to easily make awesome
object-capability-secure peer-to-peer programs!  Furthermore, the
OCapN implementation is compatible with Racket Goblins, allowing both
flavors of Goblins to interopate with each other.

**** Vats are now record types supporting custom event loops

Vat objects used to be procedures that could be passed various
arguments to interact with the underlying fiber managing the vat.
Now, vat objects are a SRFI-9 record type and can be inspected using
procedures like =vat-name=.  =call-with-vat=/=with-vat= provide the
new interface for applying thunks within a vat.

Additionally, the default fibers vat implementation has been separated
from the core vat code, allowing custom vats to be implemented on top
of other asynchronous event loops.

See the manual for full details.

**** New ,enter-vat REPL meta-command

The new =,enter-vat= meta-command, which is automatically added to
REPLs when the =(goblins)= module is imported, allows evaluating code
within the context of a specific vat.  Errors generated within a vat
while evaluating an expression are propagated to the REPL for
debugging.

**** Expanded documentation

Much of the API surface has now been documented, including several
actor-lib modules and the OCapN API.  Additional examples and
instructions for setting up the Tor daemon have been added, as well.

**** More actor-lib libraries ported from Racket version of Goblins

New modules:

 - =(goblins actor-lib facets)= for attenuating capability methods
 - =(goblins actor-lib joiners)= for resolving one or multiple promises
   (=all-of= and =all-of*= anyway, =any-of= still needs to be ported)
 - =(goblins actor-lib pubsub)= for publish-subscribe
 - =(goblins actor-lib sealers)= for actor-based sealers/unsealers
 - =(goblins actor-lib ticker)= for a collection tool useful for game
   engines with many objects that need to be updated in a tick

*** Bug fixes

 - Message order across vats was previously incorrect after completion
   of a churn
 - =extend-methods= previously re-executed the extended code upon every
   invocation which was incorrect and error-prone
 - Various fixes to syrup library

*** Known issues

**** Fibers and backtrace printing

guile-fibers >= 1.1.0 has a bug related to exception handling that
causes backtrace printing to fail.  Goblins has worked around this by
omitting backtrace printing in certain situations.  See
https://github.com/wingo/fibers/issues/76 for more information.

** v0.6

This is the first release of Spritely Goblins on Guile.
There's not much to say in terms of "differences" from the previous
version of this package, since this is the first release.  Relative
to the [[https://docs.racket-lang.org/goblins/index.html][Racket version]], this library is at a fairly early stage.

That said, much of the functionality is here: actors/objects,
synchronous and transactional invocation via =$=, asynchronous
message passage via =<-=, promise pipelining, etc etc.

The main things that are missing are:

 - Easier ways to start up a vat for a more serious program (not just
   experimenting at the REPL)
 - A more fleshed out actor-lib
 - Most significantly, a working version of CapTP.

More soon.
